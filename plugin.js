const babel = require('@babel/core');
const sqrl = require('squirrelly');
const fs = require('fs');

module.exports = ({types: t}) => {
    return {
        visitor: {
            ClassDeclaration(path) {
                if (path.node.decorators && path.node.decorators.length == 1) {
                    let decVisitor = {
                        Decorator(decoratorPath) {
                            if (decoratorPath.node.expression.callee.name == 'Squirrelly') {
                                let url = decoratorPath.node.expression.arguments[0].properties[0].value.value;
                                let rawTemplate = fs.readFileSync(url).toString();
                                let templateFnStr = sqrl.Compile(rawTemplate).toString();
                                let sqrlTemplateFn = babel.parse(templateFnStr).program.body[0];
                                let node = t.classMethod('method', t.Identifier('templateFn'), sqrlTemplateFn.params, sqrlTemplateFn.body)
                                path.get('body').pushContainer('body', node);
                                decoratorPath.remove();
                            }
                        }
                    }
                    path.traverse(decVisitor);
                }
            }
        }
    }
}